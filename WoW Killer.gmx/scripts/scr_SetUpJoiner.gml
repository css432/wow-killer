// Set up port and IP
var ip, port;
ip = argument0;
port = 3112;

// Create socket
oWaitingLobby.Socket = network_create_socket(network_socket_tcp);

if (oWaitingLobby.Socket < 0){
    show_debug_message("Socket error");
}

isConnected = network_connect(oWaitingLobby.Socket, ip, port);