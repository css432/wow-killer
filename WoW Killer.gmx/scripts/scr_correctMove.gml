
var correctAngle = Caravan_obj.image_angle;
var movement = 32 * argument0;

var curX = Caravan_obj.x;
var curY = Caravan_obj.y; 
switch (correctAngle )
{ //North -y is forward, +y is backward, -x is left, +x is right 
    case 0:
        scr_moveCaravan(curX, (curY - movement));
        break;
//East -y is left, +y is right, -x is back, +x is forward 
    case 270:
        scr_moveCaravan((curX + movement), curY );
        break;
//South -y is backward, +y is forward, -x is right, +x is left 
    case 180:
        scr_moveCaravan(curX, (curY + movement));
        break;

//West -y is right, +y is left, -x is forward, +x is backward 
    case 90:
        scr_moveCaravan((curX - movement), curY );
        break;
}
