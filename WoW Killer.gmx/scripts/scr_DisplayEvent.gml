eventID = argument0;

// Switch case based on event
switch (eventID){

    case 0: // HEAVENS TAKE THE WHEEL
        Events_obj.sprite_index = Event0_spr;
        Events_obj.visible = true;
        break;
        
    case 1: // TIME WARP
        Events_obj.sprite_index = Event1_spr;
        Events_obj.visible = true;
        break;
        
    case 2: // ENGINE FAILURE
        Events_obj.sprite_index = Event2_spr;
        Events_obj.visible = true;
        break;
        
    case 3: // DRAGON BLOODLUST
        Events_obj.sprite_index = Event3_spr;
        Events_obj.visible = true;
        break;

    case 4: // SLIGHT BREEZE
        Events_obj.sprite_index = Event4_spr;
        Events_obj.visible = true;
        break;
    
    case 5: // WE HAVE TO GO BACK
        Events_obj.sprite_index = Event5_spr;
        Events_obj.visible = true;
        break;
        
    case 6: // MISCOMMUNICATION
        Events_obj.sprite_index = Event6_spr;
        Events_obj.visible = true;
}