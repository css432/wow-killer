
// Get Event ID and display it
eventID = argument0;
scr_DisplayEvent(eventID);

// Switch case based on event
switch (eventID){

    case 0: // HEAVENS TAKE THE WHEEL - Erase all their precious runes. Mwuah ha ha
        ds_list_clear(oPlayer.RUNES_LIST);
        
        // Get random runes
        newRune1 = choose(1,2,3,4,5,6,7,8,9,10,11,12,13,14);
        newRune2 = choose(1,2,3,4,5,6,7,8,9,10,11,12,13,14);
        newRune3 = choose(1,2,3,4,5,6,7,8,9,10,11,12,13,14);
        newRune4 = choose(1,2,3,4,5,6,7,8,9,10,11,12,13,14);
        
        // Add it to RUNE_LIST
        ds_list_add(oPlayer.RUNES_LIST, newRune1, newRune2, newRune3, newRune4);
        break;
    
        
    case 1: // TIME WARP - Timer += 2
        break;
        
    case 2: // ENGINE FAILURE - Timer -= 2
        break;
        
    case 3: // DRAGON BLOODLUST
        scr_UpdateDragon(); // May need to check if game is over when UpdateDragon returns
        break;

    case 4: // SLIGHT BREEZE - nothing happens
        break;
    
    case 5: // WE HAVE TO GO BACK - Client does U TURN
        break;
        
    case 6: // MISCOMMUNICATION - Runes are shuffled
        ds_list_shuffle(oPlayer.RUNES_LIST);
}