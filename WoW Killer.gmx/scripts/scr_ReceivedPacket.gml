// arg 0 = buffer
// arg 1 = socket
var msgid = buffer_read(argument0, buffer_u8);

switch(msgid){
    // Read time from buffer, write message ID (1) to the write buffer, and
    // write the current time into the write buffer. 
    case 1:
        var time = buffer_read(argument0, buffer_u32);
        buffer_seek(argument0, buffer_seek_start, 0);
        buffer_write(argument0, buffer_u8, 1);
        buffer_write(argument0, buffer_u32, time);
        
        // Send buffer to client
        network_send_packet(argument1, argument0, buffer_tell(argument0));
        break;
}