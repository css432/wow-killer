/* 
CALLED BY:      oPlayer
PARAM:          argument0, 1, 2, 3 = runes to execute (in order)
PURPOSE:        Execute the runes and update the caravan.

NOTES:          Any text/graphic displays must be done in the DRAW event.
                An individual rune has "steps". For example, for
                Move 3 Forward, the steps are "move 1 forward" x 3.
                We need to split the steps to check if the Caravan is out
                of the game boundaries. If so, we want them to make a UTURN
                before executing the other steps.
                
RUNES:          0 == Move 3 Forward
                1 == Move 2 Forward, Turn Left
                2 == Move 2 Forward, Turn Right
                3 == Turn Left, Move 2 Forward
                4 == Turn Right, Move 2 Forward
                5 == Turn Left
                6 == Turn Right
                7 == Move 1 Forward, Turn Left, Move 1 Forward
                8 == Move 1 Forward, Turn Right, Move 1 Forward
                9 == U TURN
                10 == U TURN, Move 1 Forward
                11 == U TURN, Move 2 Forward
                12 == Turn Left, Move 1 Forward, U TURN
                13 == Turn Right, Move 1 Forward, U TURN

                
                Feel free to add your own. Just let me know so I can
                add more sprites.
   ---------------------------------------------------------------------------
*/


    //we are full
var runeNumber = argument0;
show_debug_message("Executing rune: " + string(runeNumber));
ANIMATING = true;
switch(runeNumber)
{

    case 0:
   //  0 == Move 3 Forward
        scr_correctMove(3);
        break;
    case 1:
 //   1 == Move 2 Forward, Turn Left
        //move forward 2
        scr_correctMove(2); //number of steps forward
        //turn Left
        scr_getCorrectDirection(-90);
        break;
    case 2:
    //2 == Move 2 Forward, Turn Right
        //move Forward 2
        scr_correctMove(2); //number of steps forward
        //turn Right
        scr_getCorrectDirection(90);//no change in direction
        break;
    case 3:
    //3 == Turn Left, Move 2 Forward
        //turn Left
        scr_getCorrectDirection(-90);
        //move Forward 2
        scr_correctMove(2); //number of steps forward
        break;
    case 4:
    //4 == Turn Right, Move 2 Forward
        scr_getCorrectDirection(90);
        scr_correctMove(2); //number of steps forward
        break;
    case 5:
    //5 == Turn Left
        scr_getCorrectDirection(-90);
        break;
    case 6:
    // 6 == Turn Right
        scr_getCorrectDirection(90);
        break;
    case 7:
    //7 == Move 1 Forward, Turn Left, Move 1 Forward
        scr_correctMove(1);
        scr_getCorrectDirection(-90);
        scr_correctMove(1);
        break;
    case 8:
    //8 == Move 1 Forward, Turn Right, Move 1 Forward
        scr_correctMove(1); 
        scr_getCorrectDirection(90);
        scr_correctMove(1);
        break;
    case 9:
    //9 == U TURN
        scr_getCorrectDirection(-180); //number of steps forward
        break;
    case 10:
    //10 == U TURN, Move 1 Forward
        scr_getCorrectDirection(-180);
        scr_correctMove(1); //number of steps forward
        break;
    case 11:
    //11 == U TURN, Move 2 Forward
        scr_getCorrectDirection(-180);
        scr_correctMove(2); 
        break;
    case 12:
    //12 == Turn Left, Move 1 Forward, U TURN
        scr_getCorrectDirection(-90);
        scr_correctMove(1); 
        scr_getCorrectDirection(-180);
        break;
    case 13:
     //13 == Turn Right, Move 1 Forward, U TURN 
        scr_getCorrectDirection(90);
        scr_correctMove(1); 
        scr_getCorrectDirection(-180);
        break;

}  

ANIMATING = false;

    

    // read the rune index
    
    // Switch case based on rune
        // Execute step of rune
        // Check if out of bounds. If so, UTURN and step one forward.
    
        // Execute step of rune
        // Check if out of bounds. If so, UTURN and step one forward.
        
        // etc...
        
 

// Done... I think?
