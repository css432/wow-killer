// Accept a rune index
var RuneType = argument0;

// If it's my turn and player hasn't sent the rune, 
// we can send the rune to the server.
if (MYTURN == true && SENTRUNE == false){

    // Create buffer and seek to beginning
    var buffer = buffer_create(1024, buffer_fixed, 1);
    buffer_seek(buffer, buffer_seek_start, 0);
    
    // Write messageID (2 == send Rune to Server)
    // Then write the Rune Type
    buffer_write(buffer, buffer_u8, 2);
    buffer_write(buffer, buffer_u8, RuneType);
    
    // Send to Local Server
    network_send_packet(oPlayer.Socket, buffer, buffer_tell(buffer));
    
    // Flags updated
    SENTRUNE = true; 
    Rune_Parent_obj.visible = false;    //rune selection made inivisible
    MYTURN = false;                     // turn over
}