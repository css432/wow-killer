// Dragon's First Turn -- Match X
// If the X locations do not match
if (Caravan_obj.x != Dragon_obj.x){

    // If the Caravan is to the right, move dragon right
    if (Caravan_obj.x > Dragon_obj.x){
        Dragon_obj.x = Dragon_obj.x + 32;
    }
    // Otherwise, move the dragon left
    else{
        Dragon_obj.x = Dragon_obj.x - 32;
    }
    
    // Check to see if game is over. If so, return 1.
    if (Dragon_obj.x == Caravan_obj.x && Dragon_obj.y == Caravan_obj.y){
        return 1;
    }
}
// If X locations match, then Dragon gets to move Y instead
else{
    if (Caravan_obj.y > Dragon_obj.y){
        Dragon_obj.y = Dragon_obj.y + 32;
    }else{
        Dragon_obj.y = Dragon_obj.y - 32;
    }
    
    if (Dragon_obj.x == Caravan_obj.x && Dragon_obj.y == Caravan_obj.y){
        return 1;
    }
}


// Dragon's Second Turn -- Match Y
if (Caravan_obj.y != Dragon_obj.y){

    if (Caravan_obj.y > Dragon_obj.y){
        Dragon_obj.y = Dragon_obj.y + 32;
    }else{
        Dragon_obj.y = Dragon_obj.y - 32;
    }
    
    if (Dragon_obj.x == Caravan_obj.x && Dragon_obj.y == Caravan_obj.y){
        return 1;
    }
}
// If Y locations match, then Dragon gets to move X instead
else{
    if (Caravan_obj.x > Dragon_obj.x){
        Dragon_obj.x = Dragon_obj.x + 32;
    }else{
        Dragon_obj.x = Dragon_obj.x - 32;
    }
    
    if (Dragon_obj.x == Caravan_obj.x && Dragon_obj.y == Caravan_obj.y){
        return 1;
    }
}