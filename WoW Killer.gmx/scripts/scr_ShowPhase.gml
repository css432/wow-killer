// CALLED BY:       oPlayer
// PARAM:           argument0 == The phase ID
// PURPOSE:         Script to display the phase text on screen.
// NOTES:           Any text/graphic displays must be done in the DRAW event.

// ---------------------------------------------------------------------------

// Accept phase ID
phaseID = argument0;

// Switch case based on phase ID
switch (phaseID)
{
    case 0: // SELECT YOUR RUNE!
            Phases_obj.sprite_index = Select_Phase;
            break;
            
    case 1: // MOVEMENT PHASE
            Phases_obj.sprite_index = Movement_Phase;
            break;
            
    case 2: // SPECIAL EVENT
            Phases_obj.sprite_index = Event_Phase;
            break;      
    
    case 3: // DRAGON'S MOVEMENT
            Phases_obj.sprite_index = Dragon_Phase;
            break;         
}